# Dual Bernoulli Gate

This project is to implement the behavior of [Mutable Instrument's Branches](https://mutable-instruments.net/modules/branches/) without a microcontroller.

## Why?

The behavior of a Bernoulli Gate lends itself to construction using discrete logic. I haven't designed any circuitry using discrete logic and viewed this as an opportunity to.

## Project status

Right now this project is working through early design efforts.

[] block diagram
[] circuit
[] PCB

## Help

We're discussing this project on ModWiggler: https://www.modwiggler.com/forum/viewtopic.php?t=268946

## License

This work is licensed under CC BY-SA 4.0.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/
