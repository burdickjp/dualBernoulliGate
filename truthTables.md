| Conditions    |   |   |   |   |   |   |   |   |   |
|:--------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|
| trigger       | 0 | 1 | 1 | 1 | 1 | 1 | 1 | 0 | 0 |
| prev out A    | - | - | - | 1 | 0 | 1 | 0 | 1 | 0 |
| prev out B    | - | - | - | 0 | 1 | 0 | 1 | 0 | 1 |
| prob < noise  | - | 0 | 1 | 0 | 0 | 1 | 1 | - | - |
| prob > noise  | - | 1 | 0 | 1 | 1 | 0 | 0 | - | - |
| latch toggle  | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 |
| flip-flop tog | - | 0 | 0 | 1 | 1 | 1 | 1 | - | - |
| Out A         | 0 | 0 | 1 | 0 | 1 | 1 | 0 | 1 | 0 |
| Out B         | 0 | 1 | 0 | 1 | 0 | 0 | 1 | 0 | 1 |
